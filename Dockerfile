FROM python:3.11-slim

ENV SHELL=/bin/bash \
	PYTHONFAULTHANDLER=1 \
	PYTHONUNBUFFERED=1 \
	PIP_DEFAULT_TIMEOUT=100 \
	PIP_DISABLE_PIP_VERSION_CHECK=1 \
	PIP_NO_CACHE_DIR=1 \
	POETRY_NO_INTERACTION=1 \
	POETRY_VIRTUALENVS_IN_PROJECT=1 \
	POETRY_VERSION=1.4.0

RUN pip install --upgrade pip && pip install "poetry==$POETRY_VERSION"

# Install gcc for dependencies that require a compiler
RUN apt-get update && apt-get install -y gcc
